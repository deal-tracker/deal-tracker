package uk.co.dealtracker.dao;

import uk.co.dealtracker.model.UserCredentials;

public interface LoginDao {
        boolean checkLogin(final UserCredentials userCredentials);
        }
