package uk.co.dealtracker.dao.impl;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import uk.co.dealtracker.dao.LoginDao;
import uk.co.dealtracker.model.UserCredentials;

import static org.junit.jupiter.api.Assertions.*;

public class LoginDaoImplTest {
    private static LoginDao loginDao;

    @BeforeAll
    public static void setup() {
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:uk/co/dealtracker/datasource" +
                "/config/data-source-config.xml");
        loginDao = context.getBean("loginDao",LoginDao.class);
    }

    /**
     * Check if LoginDao is instantiated successfully
     */
    @Test
    public void testLoginDaoNotNull() {
        assertTrue(loginDao != null);
    }

    /**
     *  Test null UserCredentials object
     */
    @Test
    public void testNullUserCredentialsObj() {
        assertDoesNotThrow(() -> loginDao.checkLogin(null));
    }

    /**
     * Should not make a DB call on null username
     */
    @Test
    public void testNullUserName() {
        assertDoesNotThrow(() -> loginDao.checkLogin(new UserCredentials(null, null)));
    }


    /**
     * Test valid username and password
     */
    @Test
    public void testValidUser() {
        assertTrue(loginDao.checkLogin(new UserCredentials("Ram","Ram")));
    }

    /**
     * Test invalid user
     */
    @Test
   public void testInvalidUser() {
        assertFalse(loginDao.checkLogin(new UserCredentials("Laxman","Laxman1")));
   }

}
