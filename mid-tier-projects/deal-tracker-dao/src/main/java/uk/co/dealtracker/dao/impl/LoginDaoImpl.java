package uk.co.dealtracker.dao.impl;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import uk.co.dealtracker.dao.LoginDao;
import uk.co.dealtracker.model.UserCredentials;

import java.util.HashMap;
import java.util.Map;

public class LoginDaoImpl implements LoginDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    public NamedParameterJdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public boolean checkLogin(UserCredentials userCredentials) {
        if ((userCredentials != null) && (userCredentials.getUserName() != null)) {
            String sql = "SELECT COUNT(user_name) FROM users WHERE user_name = :userName AND password = :password";
            Map<String, Object> queryParams = new HashMap<String, Object>(2);

            queryParams.put("userName", userCredentials.getUserName());
            queryParams.put("password", userCredentials.getPassword());

            Integer queryResult = getJdbcTemplate().queryForObject(sql,queryParams,Integer.class);
            boolean found = queryResult>0;
            return found;
        }
        return false;
    }

}
